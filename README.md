# DS2023 30442 Matei Octavian Mihai Assignment 1

## Description of the project

This project is aimed at creating a distributed system with:

- Frontend Project
- User interaction Project
- Product interation Project
- One database per each backend project

In order to do implement these, we will use the following technologies:

- `React` for frontend
- `Spring` and `Java` for backends
- `PostgresSQL` for the database
- `Docker` and `Docker Desktop` for the deployment

The goal is to create a website that allows the users to be split into two, an administrator and multiple clients. Each user has different ways of interacting with our system as follows:

### `Client`

The client can register, login and only see its own devices

### `Administrator`

The administrator has more actions such as:

- `CRUD` on all users
- `CRUD` on all devices
- The ability to `pair` users to devices

<div style="page-break-after: always;"></div>

## Conceptual architecture of the distributed system

![Alt text](Images/ConceptualDiagram.png)
*Fig 1: Conceptual Diagram*

As it can be seen in the image `Fig 1`, The architecture of the project is made up of 3 main elements:

### `The Frontend`

The frontend is a REACT application and it is composed of 2 main components, the Authentification part and the Main Pages part.

* The authentification communicates directly with the User backend and each user is assigned a `JWT` token to be sure that users cannot access other's accounts and each and everyone of the users is doing what the system was designed to do for them. With this, we can also ensure that an administrator is who they say they are, in order for a random user not to do CRUD operations on our DB

* The Main Pages are composed of 4 grand components. 
  * A `CommonUserTable` that acts as the main page for normal users
  * A `ProductTable` that ensures the admin can do CRUD operations on product DB table
  * A `UserTable` that ensures the admin can do CRUD operation on the user DB table
  * A `ProductUser` that ensures that the pairing can be done

The interaction between the two DBs will be discussed in the following subchapters.

### `The Backends`

The backends are Spring applications. They are composed of:

1. `User Backend` that has controllers and services related to `CRUD` operations on the user part of the project
2. `Product Backend` that has controllers and services related to `CRUD` operation on the product part of the project

Because the product part has the relationship between users and products, a special controller was built into `Product Backend` in order to allow `POST` and `DELETE` HTTP Mappings for a simplified User table that mirrors the one in the user table.

### `The DataBases`

The databases are created in PostgresSQL and have the following structure:
* `User DB` which stores the data for the users and has the following table:
  * User Table that contains user information such as email, encrypted password and some description
* `Product DB` which stores the data for the products and has the following tables:
  * Product Table that contains product information such as name and description
  * User Table that contains only the user's ids fetched from the `User DB`
  * User Product Table that contains pairs of ids Product id - User id

<div style="page-break-after: always;"></div>

## UML Deployment diagram

![Alt text](Images/DeploymentDiagram.png)
*Fig 2: Deployment Diagram*

As it can be seen in `Fig 2`, the deployment is made in Docker having each component of the project in a separate `container`. This allows for greater customizability for each. The main difference in the implementation is that during deployment and actual run of the project, the urls are changed from `localhost` to the one that the component interacts with and the ports are changed according to the one that is mapped in the file. For example:

* For the product backend to access their database, we change:
  * from `localhost:5432\product_db` to `productpostgres\product_db` which is on port `1002`
  * from `localhost:5432\user_db` to `userpostgres\product_db` which is on port `1001`

* For the communication between backends, we needed to do the following change:
  * from `http://localhost:8082/api/usertable/..` to `http://backendproduct:8082/api/usertable/..`


## Build and execution considerations

Open cmd in the root folder and run


        docker-compose up --build


After the run, a set of containers should be deployed. In order to see, I recommend using Docker Desktop.


In order to access the page, go to:


        http://localhost:3000/


or:


        http://localhost:3000/login


The data for the databases is stored in `root/init_db/...` which is ran at initialization of the database containers

In order to access the already registered users, use their email as password. For example:


        email = admin@admin.com, password = admin@admin.com
        email = dada@gmail.com, password = dada@gmail.com

## Conclusions

In order to create scalable applications that can employ a varied number of people and different technologies, the usage of Docker and container style deployment is necessary. The current project is rather simple in its scope and use, but in reality there can be a variety of services, each written in a different language and uses different types of server, which would be imposible to implement with a monolitic architecture in mind.