import React, { useState, useEffect } from 'react'

import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography
} from '@mui/material'
import config from '../../config.json'
import LocalStorageHelper from '../../common/localStorageMethods'

function CommonUserTable () {
  const [data, setData] = useState([])

  useEffect(() => {
    // Fetch data from your API when the component mounts
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }
    fetch(
      config.productApi +
        'userproduct/get_by_id/' +
        LocalStorageHelper.getUser().id,
      requestOptions
    )
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setData(response.data)
      })
      .catch()
  }, [data])

  return (
    <div>
      <div>
        <Typography variant='h3' gutterBottom>
          List of Products
        </Typography>
      </div>
      <TableContainer
        component={Paper}
        sx={{ maxHeight: 440, maxWidth: '90%', margin: 'auto' }}
      >
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id}>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.productName}</TableCell>
                <TableCell>{row.description}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default CommonUserTable
