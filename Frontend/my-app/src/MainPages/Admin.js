'use client'
import * as React from 'react'
import UserTable from './componenets/UserTable'
import ProductTable from './componenets/ProductTable'
import ProductUser from './componenets/ProductUser'
/*
    Do CRUD on clients
    Do CRUD on devices
    Do pairing on client-device
    Propagate on everything
*/

export default function Admin () {
  return (
    <div>
      <UserTable />
      <ProductTable />
      <ProductUser />
    </div>
  )
}
