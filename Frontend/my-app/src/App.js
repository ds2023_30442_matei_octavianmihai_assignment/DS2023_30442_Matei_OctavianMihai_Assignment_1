import './App.css'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from 'react-router-dom'

import Login from './login/Login'
import Register from './login/Register'
import Main from './MainPages/Main'

function App () {
  return (
    <Router>
      <Routes>
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path='/view' element={<Main />} />
        <Route path='*' exact element={<Navigate replace to='/login' />} />
      </Routes>
    </Router>
  )
}

export default App
