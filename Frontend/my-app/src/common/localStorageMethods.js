export default class LocalStorageHelper {
  static logout = () => {
    localStorage.removeItem('user')
  }

  static getUser = () => {
    return JSON.parse(localStorage.getItem('user'))
  }

  static setUser = user => {
    localStorage.removeItem('user')
    console.log(JSON.stringify(user))
    localStorage.setItem('user', JSON.stringify(user))
  }
}
