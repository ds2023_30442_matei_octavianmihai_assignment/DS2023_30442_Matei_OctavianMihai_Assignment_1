package com.utcn.product.Services.Services;

import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Common.Mapper.MapStructMapperImpl;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ArrayList<ProductDto> getAll(){
        ArrayList<Product> productArrayList = productRepository.findAll();
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();

        return mapStructMapper.productArrayToProductDtoArray(productArrayList);

    }

    public ProductDto updateUser(ProductDto productDto) throws Exception{

        Product productClass;
        if(productDto.getId() != null){
            Optional<Product> optionalProduct = productRepository.findById(productDto.getId());
            productClass = optionalProduct.orElseGet(Product::new);
        }else {
            productClass = new Product();
        }
        productClass.setProductName(productDto.getProductName());
        productClass.setDescription(productDto.getDescription());
        productRepository.saveAndFlush(productClass);

        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        return mapStructMapper.productToProductDto(productClass);
    }

    public void deleteProduct(ProductDto productDto) throws InvalidDataException {
        Optional<Product> optionalProduct = productRepository.findById(productDto.getId());

        if(optionalProduct.isPresent()){
            try {
                productRepository.deleteById(productDto.getId());
            }catch (Exception ex){
                throw new InvalidDataException("Cannot delete now");
            }
        }else{
            throw new InvalidDataException("No product found");
        }
    }

}
