package com.utcn.product.Common.Mapper;

import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Data.dto.UserTableDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.entity.UserProduct;
import com.utcn.product.Data.entity.UserTable;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.ArrayList;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapper {

    ProductDto productToProductDto(Product product);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Product updateProductFromProductDto(ProductDto productDto, @MappingTarget Product product);

    ArrayList<ProductDto> productArrayToProductDtoArray(ArrayList<Product> products);

    UserProduct userProductDtoToUserProduct(UserProductDto userProductDto);

    UserProductDto userProductToUserProductDto(UserProduct userProduct);

    ArrayList<UserProductDto> userProductListTouserProductDtoList(ArrayList<UserProduct> userProducts);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserProduct updateUserProductFromUserProductDto(UserProductDto userProductDto, @MappingTarget UserProduct userProduct);


    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    UserTable updateUserTableFromUserTableDto(UserTableDto userTableDto, @MappingTarget UserTable userTable);
}
