package com.utcn.product.Data.repository;

import com.utcn.product.Data.entity.UserTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserTable,Long> {
}
