package com.utcn.product.Services.controller;

import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.UserProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/userproduct")
public class UserProductController {

    private final UserProductService userProductService;

    public UserProductController(UserProductService userProductService) {
        this.userProductService = userProductService;
    }

    @GetMapping("/get_all")
    public ResponseEntity<ApiResponse> getAll() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            ArrayList<UserProductDto> allRelationships = userProductService.getAll();
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(allRelationships)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @GetMapping("/get_by_id/{id}")
    public ResponseEntity<ApiResponse> GetByUserId(@PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            ArrayList<ProductDto> allRelationships = userProductService.getAllByUserId(id);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(allRelationships)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @PatchMapping("/update")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody UserProductDto userProductDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            userProductService.updateRelationship(userProductDto);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(true)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<ApiResponse> deleteProduct(@RequestBody UserProductDto userProductDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            userProductService.deleteRelationship(userProductDto);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(true)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
}
