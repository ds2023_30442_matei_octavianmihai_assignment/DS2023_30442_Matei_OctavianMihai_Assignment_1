package com.utcn.product.Services.controller;

import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.ProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get_all_products")
    public ResponseEntity<ApiResponse> getAllUsers() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            ArrayList<ProductDto> allProducts = productService.getAll();
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(allProducts)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
    @PatchMapping("/update_product")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody ProductDto productDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            productService.updateUser(productDto);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(true)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete_product")
    public ResponseEntity<ApiResponse> deleteProduct(@RequestBody ProductDto productDto) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            productService.deleteProduct(productDto);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .withData(true)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

}
