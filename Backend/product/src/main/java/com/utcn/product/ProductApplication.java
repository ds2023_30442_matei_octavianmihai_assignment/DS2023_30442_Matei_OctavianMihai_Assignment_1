package com.utcn.product;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@ComponentScan({ "com.utcn.product.**" })
@PropertySource("classpath:product.properties")
public class ProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return  new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(@NotNull CorsRegistry registry) {
				registry.addMapping("/**") // Allow CORS for all endpoints
						.allowedOrigins("*") // Allow requests from any origin
						.allowedMethods("*") // Allow all HTTP methods (GET, POST, etc.)
						.allowedHeaders("*"); // Allow all headers
			}
		};
	}

}
