package com.utcn.product.Data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_product", schema="product_db")
public class UserProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "userproduct_generator")
    @SequenceGenerator(name="userproduct_generator", sequenceName = "userproduct_seq", allocationSize=1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "product_id", nullable = false)
    private Long productId;
}
