package com.utcn.product.Services.controller;

import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.UserTableService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/usertable")
@CrossOrigin("*")
public class UserTableController {

    private final UserTableService userService;

    public UserTableController(UserTableService userService) {
        this.userService = userService;
    }

    @PostMapping("/add/{id}")
    public ResponseEntity<ApiResponse> AddId(@PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            userService.addId(id);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> DeleteId(@PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            userService.removeId(id);
            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
}
