package com.utcn.product.Data.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Getter
@Setter
@Service
public class UserTableDto implements Serializable {
    private Long id;

    @Override
    public String toString() {
        return "UserTableDto{" +
                "id=" + id +
                '}';
    }
}
