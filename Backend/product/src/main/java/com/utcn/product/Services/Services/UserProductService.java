package com.utcn.product.Services.Services;

import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Common.Mapper.MapStructMapperImpl;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Data.entity.Product;
import com.utcn.product.Data.entity.UserProduct;
import com.utcn.product.Data.entity.UserTable;
import com.utcn.product.Data.repository.ProductRepository;
import com.utcn.product.Data.repository.UserProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserProductService {
    private UserProductRepository userProductRepository;
    private ProductRepository productRepository;

    public UserProductService(UserProductRepository userProductRepository, ProductRepository productRepository) {
        this.userProductRepository = userProductRepository;
        this.productRepository = productRepository;
    }

    public ArrayList<UserProductDto> getAll(){
        ArrayList<UserProduct> userProducts = (ArrayList<UserProduct>) userProductRepository.findAll();

        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        return mapStructMapper.userProductListTouserProductDtoList(userProducts);
    }

    public ArrayList<ProductDto> getAllByUserId(Long id){
        ArrayList<UserProduct> userTableArray = userProductRepository.findByUserId(id);
        ArrayList<Product> products = new ArrayList<>();
        for(UserProduct userProduct : userTableArray){
            Optional<Product> product = productRepository.findById(userProduct.getProductId());
            product.ifPresent(products::add);
        }
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        return mapStructMapper.productArrayToProductDtoArray(products);
    }

    public void updateRelationship(UserProductDto userProductDto) {
        Optional<UserProduct> userProductOptional = userProductRepository.findByRelationship(userProductDto.getId());
        UserProduct userProduct;
        if(userProductOptional.isPresent()){
            userProductOptional.get().setUserId(userProductDto.getUserId());
            userProductOptional.get().setProductId(userProductDto.getProductId());
            userProduct = userProductOptional.get();
        }else{
            MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
            userProduct = mapStructMapper.userProductDtoToUserProduct(userProductDto);
        }
        userProductRepository.saveAndFlush(userProduct);
    }

    public void deleteRelationship(UserProductDto userProductDto){
        Optional<UserProduct> userProductOptional = userProductRepository.findById(userProductDto.getId());
        userProductOptional.ifPresent(userProduct -> userProductRepository.delete(userProduct));
    }
}
