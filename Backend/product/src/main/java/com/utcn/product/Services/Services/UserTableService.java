package com.utcn.product.Services.Services;

import com.sun.istack.NotNull;
import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Data.entity.UserTable;
import com.utcn.product.Data.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserTableService {

    private UserRepository userRepository;

    public UserTableService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addId(@NotNull Long userId) throws Exception {
        Optional<UserTable> userTable = this.userRepository.findById(userId);
        if(userTable.isPresent()){
            throw new InvalidDataException("User already in db");
        }
        UserTable userTable1 = new UserTable();
        userTable1.setId(userId);
        userRepository.saveAndFlush(userTable1);
    }

    public void removeId(@NotNull Long userId) throws Exception {
        Optional<UserTable> userTable = this.userRepository.findById(userId);
        if(userTable.isEmpty()){
            throw new InvalidDataException("User not in db");
        }
        userRepository.deleteById(userId);
    }
}
