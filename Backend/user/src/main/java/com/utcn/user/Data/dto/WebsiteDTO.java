package com.utcn.user.Data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Getter
@Setter
@Service
public class WebsiteDTO implements Serializable {
    private Long id;
    private String email;
    private String jwt;
    private Boolean isAdmin;

    @Override
    public String toString() {
        return "WebsiteDTO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", jwt='" + jwt + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
