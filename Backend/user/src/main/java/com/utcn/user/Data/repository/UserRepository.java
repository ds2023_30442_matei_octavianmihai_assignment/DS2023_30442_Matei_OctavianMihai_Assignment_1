package com.utcn.user.Data.repository;

import com.utcn.user.Data.entity.CommonUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface UserRepository extends JpaRepository<CommonUser, Long> {

    @Query("Select user from CommonUser user where user.email=:email")
    CommonUser findByEmail(String email);

    @Query("select user from CommonUser user order by user.id")
    ArrayList<CommonUser> findAll();

}
