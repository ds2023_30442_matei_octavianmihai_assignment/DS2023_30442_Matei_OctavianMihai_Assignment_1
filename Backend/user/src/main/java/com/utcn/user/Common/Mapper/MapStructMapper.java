package com.utcn.user.Common.Mapper;

import com.utcn.user.Data.dto.CommonUserDto;
import com.utcn.user.Data.entity.CommonUser;
import org.mapstruct.Mapper;

import java.util.ArrayList;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapper {
    CommonUserDto userToUserDto(CommonUser commonUser);

    CommonUser userDtoToUser(CommonUserDto commonUserDto);

    ArrayList<CommonUserDto> userListToUserDtoList(ArrayList<CommonUser> commonUserArrayList);
}
