package com.utcn.user.Services.Services;

import com.sun.istack.NotNull;
import com.utcn.user.Common.Exceptions.InvalidDataException;
import com.utcn.user.Common.Exceptions.NotFoundException;
import com.utcn.user.Common.Mapper.MapStructMapperImpl;
import com.utcn.user.Data.dto.CommonUserDto;
import com.utcn.user.Data.dto.WebsiteDTO;
import com.utcn.user.Data.entity.CommonUser;
import com.utcn.user.Data.repository.UserRepository;
import com.utcn.user.Services.Security.JwtTokenUtil;
import com.utcn.user.Services.Validators.UserValidator;
import org.springframework.core.NestedRuntimeException;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Objects;

@Service
public class UserService {

    private final String productLink;

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenUtil jwtTokenUtil;

    public UserService(Environment environment, UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenUtil jwtTokenUtil) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenUtil = jwtTokenUtil;
        this.productLink = environment.getProperty("product.url");
    }

    public WebsiteDTO createUser(@NotNull CommonUserDto userDto) throws Exception {
        UserValidator.isUserValid(userDto, userRepository);
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        CommonUser userClass = mapStructMapper.userDtoToUser(userDto);
        userClass.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userRepository.saveAndFlush(userClass);

        try {
            String result = this.addIdToOtherServer(userClass.getId());
        }catch (Exception ex){
            userRepository.deleteById(userClass.getId());
            throw new InvalidDataException("Cannot add user");
        }

        WebsiteDTO websiteDTO = new WebsiteDTO();
        websiteDTO.setId(userDto.getId());
        websiteDTO.setEmail(userDto.getEmail());
        websiteDTO.setJwt(jwtTokenUtil.createToken(userDto));
        websiteDTO.setIsAdmin(userDto.getEmail().equals("admin@admin.com"));

        return websiteDTO;
    }

    public WebsiteDTO findUser(String email, String password) throws Exception {
        CommonUser userClass = userRepository.findByEmail(email);
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        if (userClass != null && passwordEncoder.matches(password, userClass.getPassword())) {

            CommonUserDto userDto = mapStructMapper.userToUserDto(userClass);

            WebsiteDTO websiteDTO = new WebsiteDTO();
            websiteDTO.setId(userDto.getId());
            websiteDTO.setEmail(userDto.getEmail());
            websiteDTO.setJwt(jwtTokenUtil.createToken(userDto));
            websiteDTO.setIsAdmin(userDto.getEmail().equals("admin@admin.com"));

            return websiteDTO;
        } else throw new NotFoundException("Invalid username or password.");
    }

    public ArrayList<CommonUserDto> getAllUsers() {
        ArrayList<CommonUser> allUsers = userRepository.findAll();

        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        return mapStructMapper.userListToUserDtoList(allUsers);
    }

    public WebsiteDTO updateUser(CommonUserDto commonUserDto) throws Exception{
        CommonUser userClass = userRepository.findByEmail(commonUserDto.getEmail());

        if (userClass != null){
            String password = commonUserDto.getPassword();
            if(!(password.isEmpty() ||
                    Objects.equals(password, "") ||
                    userClass.getPassword().equals(password)
            )) {
                userClass.setPassword(passwordEncoder.encode(password));
            }
            userClass.setDescription(commonUserDto.getDescription());
            userRepository.saveAndFlush(userClass);
        }else{
            createUser(commonUserDto);
        }

        WebsiteDTO websiteDTO = new WebsiteDTO();
        websiteDTO.setId(commonUserDto.getId());
        websiteDTO.setEmail(commonUserDto.getEmail());
        websiteDTO.setJwt(jwtTokenUtil.createToken(commonUserDto));
        websiteDTO.setIsAdmin(commonUserDto.getEmail().equals("admin@admin.com"));
        return websiteDTO;
    }

    public void deleteUser(CommonUserDto commonUserDto) throws InvalidDataException {
        if(commonUserDto.getEmail().equals("admin@admin.com")){
            throw new InvalidDataException("Cannot delete this!");
        }
        CommonUser userClass = userRepository.findByEmail(commonUserDto.getEmail());
        if (userClass != null){
            try {
                String result = this.removeIdToOtherServer(userClass.getId());
                userRepository.deleteById(userClass.getId());
            }catch (Exception ex){
                throw new InvalidDataException("Cannot delete now");
            }
        }else{
            throw new InvalidDataException("No user found!");
        }
    }

    private String addIdToOtherServer(Long id) throws NestedRuntimeException{
        final String uri = productLink + "/add/" + id.toString();

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.exchange(uri, HttpMethod.POST, null, String.class).toString();
    }

    private String removeIdToOtherServer(Long id) throws NestedRuntimeException {
        final String uri = productLink + "/delete/" + id.toString();

        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class).toString();
    }
}
